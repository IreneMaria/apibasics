package com.tm.steps;

import java.util.LinkedHashMap;

import org.junit.Assert;

import com.tm.helpers.ConfigReader;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class Steps {
	Response response;
	RequestSpecification httpRequest;
	ResponseBody body;
	JsonPath jsonBody;

	@Given("the tm API services are up and running")
	public void the_tm_api_services_are_up_and_running() {
		RestAssured.baseURI = new ConfigReader().getURL();
	}

	@When("a User performs Get request for a given {string} endpoint")
	public void a_user_performs_get_request_for_a_given_endpoint(String url) {
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, url);
	}

	@Then("the fetched API response should be {int}")
	public void the_fetched_api_response_should_be(Integer ExpectedstatusCode) {

		Integer statusCode = response.getStatusCode();
		System.out.println("Response status code is " + statusCode);
		Assert.assertEquals("Status code is not matching with expected value", ExpectedstatusCode, statusCode);

	}

	@Then("the tm API should return proper json response")
	public void the_tm_api_should_return_proper_json_response() {

		body = response.getBody();
		body.prettyPrint();

	}

	@Then("i verify the Name value as {string}")
	public void i_verify_the_name_value_as(String expectedValue) {

		String strBody = body.asString();
		jsonBody = JsonPath.from(strBody);

		Assert.assertEquals("Name value is not mathing with expected", expectedValue, jsonBody.get("Name"));

	}

	@Then("i assert the CanListClassifieds value to be false")
	public void i_assert_the_can_list_classifieds_value_to_be_false() {

		Assert.assertFalse("The CanListClassifieds value is not as expected", jsonBody.get("CanListClassifieds"));

	}

	@Then("i verify the Charities {string} and {string}")
	public void i_verify_the_charities_and(String Desc, String tagline) {

		LinkedHashMap<String, String> individualCharity = jsonBody.get("Charities[1]");

		Assert.assertEquals(Desc, individualCharity.get("Description"));

		Assert.assertTrue("The string doesot contains Expected String",
				individualCharity.get("Tagline").contains(tagline));


	}

}
