package com.tm.helpers;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;



public class ConfigReader {

	Properties prop;

	public ConfigReader() {
		try {
			File src = new File("./environment.properties");
			
			FileInputStream fis = new FileInputStream(src);

			prop = new Properties();
			prop.load(fis); 

		} catch (Exception e) {

			System.out.println(e.getMessage());
		}

	}
	

	
	public String getURL(){
		return prop.getProperty("api.url");
		
	}
	public String getDataFromConfigFile(String configFileData){
		String dataFromConfig = prop.getProperty(configFileData);
		return dataFromConfig;
		
	}
	
	
}
