package com.tm.test;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = ".", glue = { "." }, tags = "@test", plugin = { "pretty" })
public class testRunner {

	
}
