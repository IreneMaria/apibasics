Feature: Automate the test for API

  @test
  Scenario Outline: Check Assertions
    Given the tm API services are up and running
    When a User performs Get request for a given "/v1/Categories/6328/Details.json?catalogue=false" endpoint
    Then the fetched API response should be 200
    And the tm API should return proper json response
    Then i verify the Name value as "Badges"
    And i assert the CanListClassifieds value to be false
    Then i verify the Charities "<Description>" and "<Tagline>"
    
    

    Examples: 
      | Description | Tagline                          |
      | Plunket     | well child health services in NZ |
